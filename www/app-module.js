import { banner } from './cordova-plugin-admob-free/admob'
import { interstitial } from './cordova-plugin-admob-free/admob'

// SET UP ADMOB

// BANNER
admob.banner.config({
	id: 'ca-app-pub-6480948783393686/2850605145',
})
admob.banner.prepare()
admob.banner.show()

// INTERSTITIAL
admob.interstitial.config({
	id: 'ca-app-pub-6480948783393686/1999777656',
})
admob.interstitial.prepare()
admob.interstitial.show()
