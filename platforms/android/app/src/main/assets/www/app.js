// FOR DEV ONLY
//localStorage.clear();

// GLOBAL VARIABLES
var myNavigator,jsonInfo,jsonRecipes,jsonIngredients,jsonLists,jsonShoppingAisles,jsonShoppingListChecks,arrMyMenu;
var currentRecipeInstructions = [];
var currentRecipeIndex = 0;
var currentListID,currentRecipeID;
var downloadingDB = 0;

// ADMOB
var admobid = {}
if (/(android)/i.test(navigator.userAgent)) {  // for android & amazon-fireos
  admobid = {
    banner: 'ca-app-pub-6480948783393686/2850605145',
    interstitial: 'ca-app-pub-6480948783393686/1999777656',
  }
} else if (/(ipod|iphone|ipad)/i.test(navigator.userAgent)) {  // for ios
  admobid = {
    banner: 'ca-app-pub-6480948783393686/2850605145',
    interstitial: 'ca-app-pub-6480948783393686/1999777656',
  }
}



// INIT
ons.ready(function() {
	myNavigator = document.querySelector('#myNavigator');
	
	// Allow the hardware back button to function as expected
    ons.disableDeviceBackButtonHandler();
	window.document.addEventListener('backbutton', function() {
	   if (myNavigator.pages.length > 1) {
		   myNavigator.popPage();
	   } else {
			ons.notification.confirm("Are you sure to close the app?").then(
				function(index) {
					if (index == 1) { // OK button - try exit
						if (navigator.app) {
							navigator.app.exitApp();
						} else if (navigator.device) {
							navigator.device.exitApp();
						} else {
							window.close();
						}
					}
				}
			);
		}
	}, false);
	
	// NAVIGATION
	document.addEventListener('init', function(event) {
	  var page = event.target;

	  if (page.id === 'home') {
	  } else if (page.id === 'list') {
		page.querySelector('ons-toolbar .center').innerHTML = page.data.title;
		page.querySelector('.listContent').innerHTML = page.data.content;
	  } else if (page.id === 'recipe') {
		page.querySelector('ons-toolbar .center').innerHTML = page.data.title;
		page.querySelector('.recipeContent').innerHTML = recipeMyListStatus(currentRecipeInstructions[currentRecipeIndex]);
		page.querySelector('.recipeIngredients').innerHTML = page.data.ingredients;
		page.querySelector('.recipe-top-middle').innerHTML = page.data.recipetopmiddle;
		var divGD = ons.GestureDetector(document.querySelector('#recipe-middle'));
		divGD.on('swipeleft', function(event) { console.log("Swipe left detected"); clickRecipeNav(1); });
		divGD.on('swiperight', function(event) { console.log("Swipe right detected"); clickRecipeNav(-1); });
		
	  } else if (page.id === 'about') {
		page.querySelector('ons-toolbar .center').innerHTML = page.data.title;
	  }
	});
	
	document.addEventListener('postpop', function(event) {
		var page = event.enterPage;
		if (page.id === 'list') {
			console.log("Popped List - re-rendering");
			page.querySelector('.listContent').innerHTML = renderMenuList();
		} else {
			//console.log("Popped page " + page.id);
		}
	});
	
	// ADMOB  
	admob.banner.config({
		id: admobid.banner,
		isTesting: true,
		autoShow: true,
	});
	admob.banner.prepare();

	admob.interstitial.config({
		id: admobid.interstitial,
		isTesting: true,
		autoShow: false,
	});
	admob.interstitial.prepare();

	/*
	document.getElementById('showAd').disabled = true
	document.getElementById('showAd').onclick = function() {
		admob.interstitial.show()
	};
	*/
	
	// Init the rest of the app
	init();
	admob.interstitial.show();
});

function init() {
	var modal = document.querySelector('ons-modal');
	modal.show();
	
	// Set value of Thermal Cooker checkbox - set to Yes if not set before
	if (localStorage.getItem('thermalCookerCheckbox') === null) {
		localStorage.setItem('thermalCookerCheckbox', 1);
	}
	console.log("ThermalCookerCheckbox = "+localStorage.getItem('thermalCookerCheckbox'));
	document.getElementById("ThermalCookerCheckbox").checked = (localStorage.getItem('thermalCookerCheckbox') == 1 ? true : false); 

	if (downloadingDB > 0) { 
		// still downloading stuff...
	} else if (localStorage.getItem('ingredients-retrieved') === null || localStorage.getItem('recipes-retrieved') === null || localStorage.getItem('mmelists-retrieved') === null || localStorage.getItem('shoppingaisles-retrieved') === null) { 
		document.getElementById("home-content").innerHTML = "<ons-list-item>No data exists yet - please sync data to use this app</ons-list-item>";
		modal.hide();
	} else {
		console.log("Reading recipes from localStorage...");
		jsonRecipes = JSON.parse(localStorage.getItem('recipes'));
		console.log("Reading ingredients from localStorage...");
		jsonIngredients = JSON.parse(localStorage.getItem('ingredients'));
		console.log("Reading lists from localStorage...");
		jsonLists = JSON.parse(localStorage.getItem('mmelists'));
		console.log("Reading shoppingaisles from localStorage...");
		jsonShoppingAisles = JSON.parse(localStorage.getItem('shoppingaisles'));
		
		if (localStorage.getItem('shoppingListCheckbox') === null) {
			jsonShoppingListChecks = new Object();
		} else {
			jsonShoppingListChecks = JSON.parse(localStorage.getItem('shoppingListCheckbox'));
		}
		console.log("Populating shopping checklist first level...");
		Object.keys(jsonShoppingAisles).forEach(function(k) {
			if (typeof jsonShoppingListChecks[jsonShoppingAisles[k].title] === 'undefined') {
				jsonShoppingListChecks[jsonShoppingAisles[k].title] = new Object();
			}
		});

		if (localStorage.getItem('myMenu') === null || localStorage.getItem('myMenu') == "") {
			arrMyMenu = [];
		} else {
			arrMyMenu = localStorage.getItem('myMenu').split(',');
			for (i=0;i<arrMyMenu.length;i++) {
				// Values come in as strings - convert to Int
				arrMyMenu[i] = parseInt(arrMyMenu[i]);
			}		
		}

		var out = "";
		
		out += '<ons-list-header>Menus</ons-list-header>';
		Object.keys(jsonLists).forEach(function(k) {
			out += '<ons-list-item tappable onclick="clickList(' + k + ')">' + jsonLists[k].title + '</ons-list-item>';
		});
		
		document.getElementById("home-content").innerHTML = out; 
		modal.hide();
	}
	//modal.hide();
}

function syncData() {
	var modal = document.querySelector('ons-modal');
	modal.show();
	
	// Clear checkboxes on shopping lists
	localStorage.removeItem('shoppingListCheckbox');
	localStorage.removeItem('ingredients-retrieved');
	localStorage.removeItem('recipes-retrieved');
	localStorage.removeItem('mmelists-retrieved');
	localStorage.removeItem('shoppingaisles-retrieved');
	
	// SYNC RECIPES
	var xhttpR = new XMLHttpRequest();
	xhttpR.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			localStorage.setItem('recipes', this.responseText);
			localStorage.setItem('recipes-retrieved',Date());
			downloadingDB--;
			init();
		}
	};
	xhttpR.open("GET", "http://www.cobaltcode.com/mme/request/?dbname=recipes", true);
	downloadingDB++;
	xhttpR.send();
	
	var xhttpI = new XMLHttpRequest();
	xhttpI.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			localStorage.setItem('ingredients', this.responseText);
			localStorage.setItem('ingredients-retrieved',Date());
			downloadingDB--;
			init();
		}
	};
	xhttpI.open("GET", "http://www.cobaltcode.com/mme/request/?dbname=ingredients", true);
	downloadingDB++;
	xhttpI.send();	

	var xhttpL = new XMLHttpRequest();
	xhttpL.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			localStorage.setItem('mmelists', this.responseText);
			localStorage.setItem('mmelists-retrieved',Date());
			downloadingDB--;
			init();
		}
	};
	xhttpL.open("GET", "http://www.cobaltcode.com/mme/request/?dbname=mmelists", true);
	downloadingDB++;
	xhttpL.send();	

	var xhttpS = new XMLHttpRequest();
	xhttpS.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			localStorage.setItem('shoppingaisles', this.responseText);
			localStorage.setItem('shoppingaisles-retrieved',Date());
			downloadingDB--;
			init();
		}
	};
	xhttpS.open("GET", "http://www.cobaltcode.com/mme/request/?dbname=shoppingaisles", true);
	downloadingDB++;
	xhttpS.send();
}

function clickSync() {
	ons.notification.confirm({message: 'Synchronising the database can take some time and will use your Internet data. It will also clear checkboxes on all shopping lists. Are you sure you want to sync now?'})
		.then(function(response) {
			if (response == 1) { syncData(); }
		});

}

function clickList(listID) {
	currentListID = listID;
	myNavigator.pushPage('list.html', {data: {title: jsonLists[listID].title,content: renderMenuList()}});
}

function renderMenuList() {
	var thisList;

	var out = '<ons-list modifier="inset">';
	out += '<ons-list-item tappable onclick="clickShoppingList(' + currentListID + ')">Shopping List</ons-list-item>';
	out += '<ons-list-item tappable onclick="clickPrepGuide(' + currentListID + ')">Prep Guide</ons-list-item>';
	out += '</ons-list>&nbsp;<ons-list modifier="inset"><ons-list-header>Recipes</ons-list-header>';

	if (currentListID == 0) {
		// My Menu
		var strJson = "[";
		for (i=0;i<arrMyMenu.length;i++) {
			if (i > 0) { strJson += ","; }
			strJson += '{"id":"' + arrMyMenu[i] + '"}';
		}
		strJson += ']';
		thisList = JSON.parse(strJson);
		if (arrMyMenu.length < 1) {
			out += '<ons-list-item>You have not added any recipes to your list</ons-list-item>';
		}
	} else {
		thisList = jsonLists[currentListID].listentries;
	}
	Object.keys(thisList).forEach(function(k) {
		if (typeof jsonRecipes[thisList[k].id] === 'undefined') {
			console.log("WARNING Recipe "+thisList[k].id+" is not published");
		} else {
			out += '<ons-list-item tappable onclick="clickRecipe(' + thisList[k].id + ')">' + jsonRecipes[thisList[k].id].title + '</ons-list-item>';
		}
	}); 

	out += '</ons-list>';

	return out;
}

function clickShoppingList(listID) {
	var out = "";
	var shoppingList = new Object();
	var thisRecipe;
	var thisList;
	
	Object.keys(jsonShoppingAisles).forEach(function(k) {
		shoppingList[jsonShoppingAisles[k].title] = new Object();
	});	
	
	if (listID == 0) {
		// My Menu
		var strJson = "[";
		for (i=0;i<arrMyMenu.length;i++) {
			if (i > 0) { strJson += ","; }
			strJson += '{"id":"' + arrMyMenu[i] + '"}';
		}
		strJson += ']';
		thisList = JSON.parse(strJson);

	} else {
		thisList = jsonLists[listID].listentries;
	}
	
	Object.keys(thisList).forEach(function(k) {
		if (typeof jsonRecipes[thisList[k].id] === 'undefined') {
			console.log("WARNING Recipe "+thisList[k].id+" is not published");
		} else {
			thisRecipe = jsonRecipes[thisList[k].id];
			for (i = 0; i < Object.keys(thisRecipe.ingredients).length; i++) {
				if (thisRecipe.ingredients[i].quantity > 0 ) {
					// Check to see if the ingredient already exists in the shopping list
					if (typeof shoppingList[jsonShoppingAisles[jsonIngredients[thisRecipe.ingredients[i].id].shoppingaisle].title][thisRecipe.ingredients[i].id] === 'undefined') {
						// Ingredient does not exist - add it
						shoppingList[jsonShoppingAisles[jsonIngredients[thisRecipe.ingredients[i].id].shoppingaisle].title][thisRecipe.ingredients[i].id] = {quantity: thisRecipe.ingredients[i].quantity,recipes: thisRecipe.title};
					} else {
						// Ingredient already exists - add to it
						shoppingList[jsonShoppingAisles[jsonIngredients[thisRecipe.ingredients[i].id].shoppingaisle].title][thisRecipe.ingredients[i].id].quantity += thisRecipe.ingredients[i].quantity;
						shoppingList[jsonShoppingAisles[jsonIngredients[thisRecipe.ingredients[i].id].shoppingaisle].title][thisRecipe.ingredients[i].id].recipes += ", " + thisRecipe.title;
					}
				}
			}
		}
	}); 

	var lastAisle = "";
	var thisQuantity;
	Object.keys(shoppingList).forEach(function(k1) {
		if (k1 != lastAisle) { 
			if (lastAisle != "") { out += '</ons-list>&nbsp;'; }
			lastAisle = k1; 
			out += '<ons-list modifier="inset"><ons-list-header>' + k1 + '</ons-list-header>'; 
		}
		Object.keys(shoppingList[k1]).forEach(function(k2) {
			thisQuantity = shoppingList[k1][k2].quantity;
			if (Math.trunc(thisQuantity) != thisQuantity) {
				thisQuantity = (Math.trunc(thisQuantity) == 0 ? "" : Math.trunc(thisQuantity))+ " " + fraction(thisQuantity - Math.trunc(thisQuantity));
			}
			if (thisQuantity == 0) { 
				thisQuantity = ""; 
			} else {
				thisQuantity = thisQuantity + (thisQuantity > 1 ? jsonIngredients[k2].plural : jsonIngredients[k2].singular);
			}
			out += '<ons-list-item><div class="left"><ons-checkbox ' + (jsonShoppingListChecks[k1][k2] ? 'checked' : 'unchecked') + ' onclick="shoppingListCheckboxChange(\''+k1+'\','+k2+',this);"></ons-checkbox>&nbsp;</div><div class="center">&nbsp;<span class="list-item__title">' + jsonIngredients[k2].title + ': ' + thisQuantity + '</span><span class="list-item__subtitle">' + shoppingList[k1][k2].recipes + '</span></div></ons-list-item>';
		}); 
	}); 
	out += '</ons-list>';

	myNavigator.pushPage('list.html', {data: {title: jsonLists[listID].title,content: out}});
}

function clickPrepGuide(listID) {
	var out = "";
	var thisList;
	var thisRecipe;
	
	if (listID == 0) {
		// My Menu
		var strJson = "[";
		for (i=0;i<arrMyMenu.length;i++) {
			if (i > 0) { strJson += ","; }
			strJson += '{"id":"' + arrMyMenu[i] + '"}';
		}
		strJson += ']';
		thisList = JSON.parse(strJson);

	} else {
		thisList = jsonLists[listID].listentries;
	}

	Object.keys(thisList).forEach(function(k) {
		if (typeof jsonRecipes[thisList[k].id] === 'undefined') {
			console.log("WARNING Recipe "+thisList[k].id+" is not published");
		} else {
			thisRecipe = jsonRecipes[thisList[k].id];
			out += "<b>" + thisRecipe.title + "</b><br />";
			for (i=0;i<thisRecipe.instructionsprep.length;i++) {
				out += thisRecipe.instructionsprep[i].text + "<br />";
			}
			out += "<br /><br />";
			}
	}); 

	myNavigator.pushPage('list.html', {data: {title: jsonLists[listID].title,content: out}});
}

function shoppingListCheckboxChange(listID,ingredientID,source) {
	console.log("Hit a checkbox - changed " + listID + "-" + ingredientID + " to " + source.checked);
	jsonShoppingListChecks[listID][ingredientID] = source.checked;
	localStorage.setItem('shoppingListCheckbox', JSON.stringify(jsonShoppingListChecks));
}

function myMenuStatusChange(targetSwitch) {
	var newText;
	var index = arrMyMenu.indexOf(currentRecipeID);
	
	if (index > -1) {
		// Entry exists, remove it
		arrMyMenu.splice(index,1);
		targetSwitch.checked = false;
	} else {
		// Entry does not exist, add it
		arrMyMenu.push(currentRecipeID);
		targetSwitch.checked = true;
	}
	localStorage.setItem('myMenu', arrMyMenu.join(','));
}

function clickRecipe(recipeID) {
	currentRecipeID = recipeID;
	var thisRecipe = jsonRecipes[recipeID];
	var thisInstructions;
	var outContent = "";
	var outIngredients = "";
	var recipestats = "";
	currentRecipeInstructions = [];
	
	currentRecipeIndex = 0;
	
	currentRecipeInstructions[0] = '<img class="recipeImage" src="' + thisRecipe.image + '" /><ons-switch onclick="myMenuStatusChange(this);"' + (arrMyMenu.indexOf(currentRecipeID) > -1 ? " checked" : "") + '></ons-switch> Add to My List<br /><br />'; //Created by ' + thisRecipe.author + '<br /><br />';
	// <ons-checkbox ' + (jsonShoppingListChecks[k1][k2] ? 'checked' : 'unchecked') + ' onclick="shoppingListCheckboxChange(\''+k1+'\','+k2+',this);"></ons-checkbox>
	//currentRecipeInstructions[0] += '<ons-icon icon="ion-ios-list-outline"></ons-icon> Show ingredients<br/>';
	currentRecipeInstructions[0] += '<ons-icon icon="ion-ios-person-outline"></ons-icon> Serves<br/>';
	currentRecipeInstructions[0] += '<ons-icon icon="ion-ios-timer-outline"></ons-icon> Prep time (minutes)<br/>';
	currentRecipeInstructions[0] += '<ons-icon icon="ion-ios-time-outline"></ons-icon> Cook time (minutes)<br/>';
	currentRecipeInstructions[0] += '<br />'+ thisRecipe.tips;
	
	if (thisRecipe.serves > 0) { recipestats += '<ons-icon icon="ion-ios-person-outline" size="30px"></ons-icon> ' + thisRecipe.serves + '&nbsp;&nbsp;&nbsp;'; }
	if (thisRecipe.preptime > 0) { recipestats += '<ons-icon icon="ion-ios-timer-outline" size="30px"></ons-icon> ' + thisRecipe.preptime + '&nbsp;&nbsp;&nbsp;'; }
	if (thisRecipe.cooktime > 0) { recipestats += '<ons-icon icon="ion-ios-time-outline" size="30px"></ons-icon> ' + thisRecipe.cooktime; }
	
	
	if (localStorage.getItem('thermalCookerCheckbox') == 1) { thisInstructions = thisRecipe.instructions; } else { thisInstructions = thisRecipe.instructionstraditional; }
	for (i=0;i<thisInstructions.length;i++) {
		currentRecipeInstructions[i+1] = thisInstructions[i].text;
	}

	outIngredients += "<p>Ingredients:<ul>";
	var lastSection = "";
	for (i=0;i<thisRecipe.ingredients.length;i++) {
		if (thisRecipe.ingredients[i].section != lastSection) { lastSection = thisRecipe.ingredients[i].section; outIngredients += '<br /><div class="ingredientSectionHeading">' + lastSection + '</div>'; }
		var thisIngredient =  jsonIngredients[thisRecipe.ingredients[i].id];
		var thisQuantity = thisRecipe.ingredients[i].quantity;
		if (Math.trunc(thisQuantity) != thisQuantity) {
			thisQuantity = (Math.trunc(thisQuantity) == 0 ? "" : Math.trunc(thisQuantity))+ " " + fraction(thisQuantity - Math.trunc(thisQuantity));
		}
		if (thisQuantity == 0) { 
			thisQuantity = ""; 
		} else {
			thisQuantity = thisQuantity + (thisRecipe.ingredients[i].quantity > 1 ? thisIngredient.plural : thisIngredient.singular);
		}
		outIngredients += "<li>" + thisQuantity + " " + thisIngredient.title;
		// if (thisRecipe.ingredients[i].notes != "") { outIngredients += ' <ons-icon style="{color: #0000FF;}" icon="md-info" size="14px" onclick="showPopOver(\'' + thisRecipe.ingredients[i].notes + '\',this)"></ons-icon>'; }
		if (thisRecipe.ingredients[i].notes != "") { outIngredients += '<br /><div class="ingredientNote">' + thisRecipe.ingredients[i].notes + '</div>'; }
		outIngredients += "</li>";
	};
	outIngredients += "</ul></p>";

	myNavigator.pushPage('recipe.html', {data: {recipeid: recipeID, title: thisRecipe.title,ingredients: outIngredients, recipetopmiddle: recipestats}});
}

function clickRecipeNav(value) {
	if ((value > 0 && currentRecipeIndex < (currentRecipeInstructions.length - 1)) || (value < 0 && currentRecipeIndex > 0)) {
		console.log("Recipe navigation " + value);
		currentRecipeIndex += value;
		document.getElementById("recipeContent").innerHTML = (currentRecipeIndex == 0 ? "" : currentRecipeIndex + "/" + (currentRecipeInstructions.length - 1) + ": ") + recipeMyListStatus(currentRecipeInstructions[currentRecipeIndex]);
	}
}

function recipeMyListStatus(text) {
	return text.replace("#ADDTEXT#",arrMyMenu.indexOf(currentRecipeID) > -1 ? "Remove from my menu" : "Add to my menu");
}

function showPopOver(content,target) {
	document.getElementById("popOverContent").innerHTML = content;
	document.getElementById("popOverObject").show(target);
}

document.addEventListener('admob.banner.events.LOAD_FAIL', function(event) {
  console.log(event)
})

document.addEventListener('admob.interstitial.events.LOAD_FAIL', function(event) {
  console.log(event)
})

document.addEventListener('admob.interstitial.events.LOAD', function(event) {
  console.log(event)
  document.getElementById('showAd').disabled = false
})

document.addEventListener('admob.interstitial.events.CLOSE', function(event) {
  console.log(event)

  admob.interstitial.prepare()
})

function HCF(u, v) { 
    var U = u, V = v
    while (true) {
        if (!(U%=V)) return V
        if (!(V%=U)) return U 
    } 
}
//convert a decimal into a fraction
function fraction(decimal){

    if(!decimal){
        decimal=this;
    }
    whole = String(decimal).split('.')[0];
    decimal = parseFloat("."+String(decimal).split('.')[1]);
    num = "1";
    for(z=0; z<String(decimal).length-2; z++){
        num += "0";
    }
    decimal = decimal*num;
    num = parseInt(num);
    for(z=2; z<decimal+1; z++){
        if(decimal%z==0 && num%z==0){
            decimal = decimal/z;
            num = num/z;
            z=2;
        }
    }
    //if format of fraction is xx/xxx
    if (decimal.toString().length == 2 && 
            num.toString().length == 3) {
                //reduce by removing trailing 0's
        decimal = Math.round(Math.round(decimal)/10);
        num = Math.round(Math.round(num)/10);
    }
    //if format of fraction is xx/xx
    else if (decimal.toString().length == 2 && 
            num.toString().length == 2) {
        decimal = Math.round(decimal/10);
        num = Math.round(num/10);
    }
    //get highest common factor to simplify
    var t = HCF(decimal, num);

    //return the fraction after simplifying it
    return ((whole==0)?"" : whole+" ")+decimal/t+"/"+num/t;
}
